# GrooBot
## Introduction
GrooBot is a discord bot, that can't do much yet, but will be able to in the future!
## I need a config.json file!?
Yes, you do! No fear, they're super simple to make! All you need to do is copy the following into a file named "config.json"
```json
{
    "token" : "Your token here",
    "prefix" : "Your Prefix Here"
}
```
### Tokens
You can get a token by going to the discord developer's site, and creating an application. They're plenty of tutorials to help you do this.

### Prefix
A prefix is a symbol or phrase the bot uses to differentiate a normal message and a bot command. For example, if you'd like !ping to be a command, your prefix is "!".

There will be more options available soon as the bot becomes more advanced!
