"use strict";
const Discord = require("discord.js");
const client = new Discord.Client();

let config;

console.log("Starting GrooBot");
console.log("Loading config.json")

try {
    config = require("./config.json");
}

catch(error){
    console.error("Some form of error happened while finding the config file. Please ensure you have made a config.json file, as found in README!");
}

//const wEnable = config.waid;

client.on("ready", function(){
    console.log(`GrooBot started successfully and connected as ${client.user.tag} on ${client.guilds.size} server(s)!`);
});

client.on("message", async message =>{
    if (message.author.bot) return;
    if (message.content.indexOf(config.prefix)!==0) return;
    const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();
    if (command == "ping"){
        const msg = await message.channel.send("PingTest?");
        m.edit(`Pong! Latency is ${msg.createdTimestamp - message.createdTimestamp}ms. API Latency is ${Math.round(client.ping)}ms`);
    }

});

client.login(config.token);
